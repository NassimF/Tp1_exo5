# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 16:39:04 2020

@author: tp
"""

from MyCalculator import SimpleCalculator


TEST = SimpleCalculator()

print("Adition de 15 et 5: " + str(TEST.to_sum(15, 5)))
print("Soustraction de 15 et 5: " + str(TEST.substract(15, 5)))
print("Multiplication de 15 par 5: " + str(TEST.multiply(15, 5)))
print("Dvision de 15 par 5: " + str(TEST.divide(15, 1)))
